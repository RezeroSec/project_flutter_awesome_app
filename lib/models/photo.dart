// To parse this JSON data, do
//
//     final photo = photoFromMap(jsonString);

import 'dart:convert';

Photo photoFromMap(String str) => Photo.fromMap(json.decode(str));

String photoToMap(Photo data) => json.encode(data.toMap());

class Photo {
    Photo({
        this.photographer,
        this.photographerUrl,
        this.photoUrl,
        this.url,
        this.alt,
    });

    String? photographer;
    String? photographerUrl;
    String? photoUrl;
    String? url;
    String? alt;

    factory Photo.fromMap(Map<String, dynamic> json) => Photo(
        photographer: json["photographer"],
        photographerUrl: json["photographer_url"],
        photoUrl: json["photo_url"],
        url: json["url"],
        alt: json["alt"],
    );

    Map<String, dynamic> toMap() => {
        "photographer": photographer,
        "photographer_url": photographerUrl,
        "photo_url": photoUrl,
        "url": url,
        "alt": alt,
    };
}
