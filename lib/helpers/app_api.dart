part of app_helper;

class APIPath with Config {
  //! getPhoto
  static String getPhoto(int page, int per_page) {
    return "https://api.pexels.com/v1/curated?page=$page&per_page=$per_page";
  }
}
