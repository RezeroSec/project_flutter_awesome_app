library app_helper;

import 'package:awesome_app/config/app_config.dart';
import 'package:flutter/material.dart';


part 'app_api.dart';
part 'app_assets_path.dart';

p(String text) {
  !Config.appModeProduction ? print(text) : null;
}


Map<int, Color> color = {
  50:const Color(0xff4ba181),
  100:const Color(0xff4ba181),
  200:const Color(0xff4ba181),
  300:const Color(0xff4ba181),
  400:const Color(0xff4ba181),
  500:const Color(0xff4ba181),
  600:const Color(0xff4ba181),
  700:const Color(0xff4ba181),
  800:const Color(0xff4ba181),
  900:const Color(0xff4ba181),
};
