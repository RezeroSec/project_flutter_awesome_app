import 'package:awesome_app/pages/home/home.dart';
import 'package:get/get.dart';

class AppRouters {
  static const initial = '/home';

  static final routes = [
    GetPage(
      name: '/home',
      page: () => const Home(),
    )
  ];
}
