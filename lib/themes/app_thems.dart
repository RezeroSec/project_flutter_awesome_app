import 'package:flutter/material.dart';
import '../helpers/app_helper.dart';

class AppTheme {
  static ThemeData get basic => ThemeData(
        fontFamily: Font.monda,
        primaryColor: const Color(0xff41C8E8),
        primarySwatch: MaterialColor(0xff4ba181, color),
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 48.0),
          headline2: TextStyle(fontSize: 36.0),
          headline3: TextStyle(fontSize: 36.0, fontWeight: FontWeight.bold),
          headline4: TextStyle(fontSize: 24.0),
          bodyText1: TextStyle(fontSize: 18.0),
        ),
        //! color for scrollbar
        scrollbarTheme: ScrollbarThemeData(
          interactive: true,
          // trackColor: MaterialStateProperty.all(const Color(0xffE4E8EB)),
          // trackBorderColor: MaterialStateProperty.all(const Color(0xffE4E8EB)),
          // showTrackOnHover: true,
          isAlwaysShown: true,
          thickness: MaterialStateProperty.all(4),
          thumbColor: MaterialStateProperty.all(const Color(0xffB6B9C0)),
          radius: const Radius.circular(5),
          minThumbLength: 50,
        ),
      );
}
