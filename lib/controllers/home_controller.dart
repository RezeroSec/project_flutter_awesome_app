import 'dart:async';

import 'package:awesome_app/config/app_config.dart';
import 'package:awesome_app/helpers/app_helper.dart';
import 'package:awesome_app/models/photo.dart';
import 'package:dio/dio.dart' as d;
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum StatusFilter {
  list,
  grid,
}

class HomeController extends GetxController {
  //! internet
  d.Dio dio = d.Dio();
  var isInternetNetworkSuccess = true.obs;
  //!
  late Timer reloadTimer;
  var listPhoto = <Photo>[].obs;
  var page = 1.obs;
  var per_page = 4.obs;
  var isInitInternetError = false.obs;
  var statusFilter = StatusFilter.list.obs;
  //!
  var isInitGridView = false;

  //! init
  @override
  // ignore: unnecessary_overrides
  void onInit() async {
    // called immediately after the widget is allocated memory
    await getImageFromAPI(page.value, per_page.value);
    if (!isInternetNetworkSuccess.value) {
      p("Tidak ada internet");
      isInitInternetError.value = true;
      reloadTimer = Timer.periodic(const Duration(seconds: 10), (timer) async {
        await getImageFromAPI(page.value, per_page.value);
        if (isInternetNetworkSuccess.value) {
          isInitInternetError.value = false;
          reloadTimer.cancel();
        }
      });
    }
    super.onInit();
  }

  @override
  // ignore: unnecessary_overrides
  void onClose() {
    reloadTimer.cancel();
    super.onClose();
  }

  //! untuk mendapatkan data image dari API
  Future<void> getImageFromAPI(int pageNumber, int perPage) async {
    try {
      dio.options.headers["authorization"] = Config.API_KEY;
      d.Response response =
          await dio.get(APIPath.getPhoto(pageNumber, perPage));
      if (response.statusCode == 200) {
        isInternetNetworkSuccess.value = true;
        page.value = page.value + 1;
        if (isInitGridView) {
          listPhoto.clear();
        }
        var data = response.data;
        var photo = data["photos"];
        for (int i = 0; i < photo.length; i++) {
          var temp = await convToObjPhoto(photo[i]);
          listPhoto.add(temp);
        }
        p("pageNumber $pageNumber");
        p("perPage $perPage");
      }
    } on d.DioError catch (ex) {
      _dioError(ex);
    }
  }

  Future<Photo> convToObjPhoto(var obj) async {
    Photo photo = Photo();
    photo.photographer = obj["photographer"];
    photo.photographerUrl = obj["photographer_url"];
    photo.photoUrl = obj["url"];
    photo.url = obj["src"]["large"];
    photo.alt = obj["alt"];

    return photo;
  }

  Future<void> initFilterGrid() async {
    if (listPhoto.length < 6) {
      isInitGridView = true;
      per_page.value = 6;
      await getImageFromAPI(1, per_page.value);
      isInitGridView = false;
      if (!isInternetNetworkSuccess.value) {
        reloadTimer =
            Timer.periodic(const Duration(seconds: 10), (timer) async {
          await getImageFromAPI(1, per_page.value);
          if (isInternetNetworkSuccess.value) {
            reloadTimer.cancel();
            p("timer off karena ada internet pada grid");
          }
          if (statusFilter.value == StatusFilter.list) {
            reloadTimer.cancel();
            p("timer off karena ada pindah ke list");
          }
        });
      }
    }
  }

  //! dio error
  _dioError(d.DioError er) {
    if (er.type == d.DioErrorType.response) {
      isInternetNetworkSuccess.value = false;
      p("error http respon 400,500");
    } else if (er.type == d.DioErrorType.other) {
      isInternetNetworkSuccess.value = false;
      p("error http default");
    } else {
      isInternetNetworkSuccess.value = false;
      p("error time out or cancel");
    }
    Get.snackbar(
      "Warning", // title
      "There is a problem with your internet connection. Please check your internet network", // message
      barBlur: 20,
      colorText: Colors.white,
      isDismissible: true,
      duration: const Duration(seconds: 3),
      snackPosition: SnackPosition.TOP,
    );
  }
}
