import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'routes/app_routers.dart';
import 'themes/app_thems.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Awesome App',
      debugShowCheckedModeBanner: false,
      theme: AppTheme.basic,
      initialRoute: AppRouters.initial,
      getPages: AppRouters.routes,
    );
  }
}
