part of detail;

class ListTileCustom extends StatelessWidget {
  final String? title;
  final String? value;

  const ListTileCustom({Key? key, this.title, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
        width: 20,
        alignment: Alignment.center,
        child: const Icon(
          Icons.donut_small,
          color: Colors.blue,
          size: 17,
        ),
      ),
      title: Row(
        children: [
          SizedBox(
            width: 130,
            child: Text(
              title!,
              style: TextStyle(
                color: Theme.of(context).colorScheme.primary,
                fontSize: 16,
              ),
            ),
          ),
          Container(
            width: 20,
            alignment: Alignment.center,
            child: Text(
              ':',
              style: TextStyle(
                color: Theme.of(context).colorScheme.primary,
                fontSize: 16,
              ),
            ),
          ),
          Expanded(
            child: AutoSizeText(
              value!,
              presetFontSizes: const [16, 15, 14, 13, 12, 11, 10],
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Theme.of(context).colorScheme.primary,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
