library detail;

import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_app/helpers/app_helper.dart';
import 'package:awesome_app/models/photo.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

//! component
part 'components/listTileCustom.dart';

class DetailInformation extends StatefulWidget {
  const DetailInformation({Key? key}) : super(key: key);

  @override
  _DetailInformationState createState() => _DetailInformationState();
}

class _DetailInformationState extends State<DetailInformation> {
  String tag = "";
  String url = "";
  var path = "";
  Photo photo = Photo();
  bool isSwapUp = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var args = Get.arguments;
    photo = args[0];
    tag = photo.alt!;
    path = args[1];
  }

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail Information"),
        centerTitle: true,
      ),
      body: SizedBox(
        width: w,
        height: h,
        child: Stack(
          children: [
            Column(
              children: [
                Hero(
                  tag: tag,
                  child: SizedBox(
                    width: w,
                    height: isSwapUp ? h * 0.7 : h - 80,
                    child: path == ""
                        ? const Icon(Icons.error)
                        : Image.file(
                            File(path),
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
                const Expanded(child: SizedBox()),
              ],
            ),
            AnimatedPositioned(
              duration: const Duration(milliseconds: 200),
              bottom: isSwapUp ? 0 : -((h * 0.35) - 110),
              right: 0,
              child: GestureDetector(
                child: Container(
                  width: w,
                  height: h * 0.35,
                  padding: const EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(w * 0.08),
                      topRight: Radius.circular(w * 0.08),
                    ),
                  ),
                  child: Column(
                    children: [
                      Container(
                        width: w,
                        height: 40,
                        alignment: Alignment.center,
                        child: Container(
                          width: w * 0.25,
                          height: 3,
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.primary,
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                      ),
                      ListTileCustom(
                        title: "Photo Name",
                        value: photo.alt,
                      ),
                      ListTileCustom(
                        title: "Photographer name",
                        value: photo.photographer,
                      ),
                      ListTileCustom(
                        title: "Photographer URL",
                        value: photo.photographerUrl,
                      ),
                      ListTileCustom(
                        title: "Photo URL",
                        value: photo.photoUrl,
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  if (!isSwapUp) {
                    setState(() {
                      isSwapUp = true;
                    });
                  }
                },
                onVerticalDragEnd: (dragEndDetails) {
                  if (dragEndDetails.primaryVelocity! < 0) {
                    //! swap up
                    setState(() {
                      isSwapUp = true;
                    });
                  } else if (dragEndDetails.primaryVelocity! > 0) {
                    //! swap down
                    setState(() {
                      isSwapUp = false;
                    });
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
