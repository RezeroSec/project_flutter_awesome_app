library home;

import 'package:awesome_app/controllers/home_controller.dart';
import 'package:awesome_app/helpers/app_helper.dart';
import 'package:awesome_app/pages/detail/detailInformation.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

//! component
part 'components/customIconButton.dart';
part 'components/customBox.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final homeController = Get.put(HomeController());

  //!
  bool isIconOnTap = false;
  List<bool> iconSelected = [true, false];
  ScrollController _scrollController = ScrollController();
  int length = 5;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        p("akhie");
        homeController.getImageFromAPI(
            homeController.page.value, homeController.per_page.value);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _selected(int index) async {
    if (index == 0) {
      iconSelected[0] = true;
      iconSelected[1] = false;
      homeController.statusFilter.value = StatusFilter.list;
      homeController.per_page.value = 4;
    } else {
      iconSelected[0] = false;
      iconSelected[1] = true;
      homeController.statusFilter.value = StatusFilter.grid;
      await homeController.initFilterGrid();
    }
  }

  Future<String> _findPath(String imageUrl) async {
    var file = await DefaultCacheManager().getSingleFile(imageUrl);
    return file.path;
  }

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return Scaffold(
      body: CustomScrollView(
        controller: _scrollController,
        slivers: [
          SliverAppBar(
            floating: false,
            pinned: true,
            expandedHeight: 150,
            flexibleSpace: FlexibleSpaceBar(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    child: Text(
                      'Awesome App',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 200),
                    width: isIconOnTap ? 105 : 50,
                    height: 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        width: 1,
                        color: isIconOnTap ? Colors.white : Colors.transparent,
                      ),
                    ),
                    child: isIconOnTap
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomIconButton(
                                icon: Icons.list,
                                onTap: () {
                                  setState(() {
                                    isIconOnTap = false;
                                    _selected(0);
                                  });
                                },
                              ),
                              CustomIconButton(
                                icon: Icons.grid_view_outlined,
                                onTap: () {
                                  setState(() {
                                    isIconOnTap = false;
                                    _selected(1);
                                  });
                                },
                              )
                            ],
                          )
                        : iconSelected[0]
                            ? CustomIconButton(
                                icon: Icons.list,
                                onTap: () {
                                  setState(() {
                                    isIconOnTap = true;
                                  });
                                },
                              )
                            : CustomIconButton(
                                icon: Icons.grid_view_outlined,
                                onTap: () {
                                  setState(() {
                                    isIconOnTap = true;
                                  });
                                },
                              ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ),
          ),
          Obx(
            () => homeController.isInitInternetError.value
                ? SliverList(
                    delegate: SliverChildListDelegate(
                      List.generate(
                        1,
                        (i) {
                          return Container(
                            width: w,
                            height: h,
                            alignment: Alignment.center,
                            child: Text(
                              "No Internet Network",
                              style: TextStyle(
                                color: Colors.grey[400],
                                fontSize: 20,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  )
                : iconSelected[0]
                    ? Obx(
                        () => SliverList(
                          delegate: SliverChildListDelegate(
                            List.generate(
                              homeController.listPhoto.length,
                              (i) {
                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 10,
                                    horizontal: 20,
                                  ),
                                  child: CustomBox(
                                    width: w,
                                    height: h * 0.5,
                                    number: i,
                                    tag: homeController.listPhoto[i].alt,
                                    url: homeController.listPhoto[i].url,
                                    onTap: () async {
                                      setState(() {
                                        isIconOnTap = false;
                                      });
                                      var path = "";
                                      try {
                                        path = await _findPath(
                                            homeController.listPhoto[i].url!);
                                      } on Exception catch (_) {
                                        p("error");
                                      }

                                      Get.to(const DetailInformation(),
                                          arguments: [
                                            homeController.listPhoto[i],
                                            path,
                                          ]);
                                    },
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      )
                    : Obx(
                        () => SliverGrid(
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 0.8,
                            // mainAxisSpacing: 5,
                            crossAxisSpacing: 5,
                          ),
                          delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int i) {
                              var left = 0.0;
                              var right = 0.0;
                              if (i % 2 == 0) {
                                left = 5;
                                right = 0;
                                // p("i => $i => genap");
                              } else {
                                left = 0;
                                right = 5;
                                // p("i => $i => ganjil");
                              }
                              return Padding(
                                padding: EdgeInsets.fromLTRB(left, 5, right, 0),
                                child: CustomBox(
                                  width: w,
                                  height: h * 0.5,
                                  number: i,
                                  tag: homeController.listPhoto[i].alt,
                                  url: homeController.listPhoto[i].url,
                                  onTap: () async {
                                    setState(() {
                                      isIconOnTap = false;
                                    });
                                    var path = "";
                                    try {
                                      path = await _findPath(
                                          homeController.listPhoto[i].url!);
                                    } on Exception catch (_) {
                                      p("error");
                                    }
                                    Get.to(const DetailInformation(),
                                        arguments: [
                                          homeController.listPhoto[i],
                                          path,
                                        ]);
                                  },
                                ),
                              );
                            },
                            childCount: homeController.listPhoto.length,
                          ),
                        ),
                      ),
          ),
        ],
      ),
    );
  }
}
