part of home;

class CustomBox extends StatefulWidget {
  final double? width;
  final double? height;
  final int? number;
  final Function()? onTap;
  final String? url;
  final String? tag;

  const CustomBox({
    Key? key,
    this.width,
    this.height,
    this.number,
    this.onTap,
    this.url,
    this.tag,
  }) : super(key: key);

  @override
  _CustomBoxState createState() => _CustomBoxState();
}

class _CustomBoxState extends State<CustomBox> {
  static final customCacheManager = CacheManager(
    Config("customCacheKey", maxNrOfCacheObjects: 50),
  );
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Hero(
        tag: widget.tag!,
        child: CachedNetworkImage(
          cacheManager: customCacheManager,
          width: widget.width,
          height: widget.height,
          fit: BoxFit.cover,
          imageUrl: widget.url!,
          imageBuilder: (context, imageProvider) => Container(
            width: 400,
            height: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                //image size fill
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          placeholder: (context, url) => SpinKitWave(
            color: Colors.grey[400],
            type: SpinKitWaveType.start,
            size: 50,
          ),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
      onTap: widget.onTap,
    );
  }
}
