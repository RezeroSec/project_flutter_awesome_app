part of home;

class CustomIconButton extends StatefulWidget {
  final Function()? onTap;
  final IconData? icon;

  const CustomIconButton({
    Key? key,
    this.onTap,
    this.icon,
  }) : super(key: key);

  @override
  _CustomIconButtonState createState() => _CustomIconButtonState();
}

class _CustomIconButtonState extends State<CustomIconButton> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(10),
      child: InkWell(
        splashColor: Colors.grey,
        onTap: widget.onTap,
        onHover: (value) {},
        child: Container(
          width: 50,
          height: 30,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Icon(
            widget.icon,
            color: Colors.white,
            size: 20,
          ),
        ),
      ),
    );
  }
}
