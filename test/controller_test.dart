import 'package:awesome_app/controllers/home_controller.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('fungsi di controller', () {
    test('pengujian convert Object Foto', () async {
      final homeController = HomeController();

      final obj = await homeController.convToObjPhoto({
        "id": 11150437,
        "width": 4016,
        "height": 6016,
        "url":
            "https://www.pexels.com/photo/dessert-with-raspberries-on-the-wooden-table-11150437/",
        "photographer": "Federica Gioia",
        "photographer_url": "https://www.pexels.com/@fedigioia",
        "photographer_id": 118637296,
        "avg_color": "#604325",
        "src": {
          "original":
              "https://images.pexels.com/photos/11150437/pexels-photo-11150437.jpeg",
          "large2x":
              "https://images.pexels.com/photos/11150437/pexels-photo-11150437.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
          "large":
              "https://images.pexels.com/photos/11150437/pexels-photo-11150437.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
          "medium":
              "https://images.pexels.com/photos/11150437/pexels-photo-11150437.jpeg?auto=compress&cs=tinysrgb&h=350",
          "small":
              "https://images.pexels.com/photos/11150437/pexels-photo-11150437.jpeg?auto=compress&cs=tinysrgb&h=130",
          "portrait":
              "https://images.pexels.com/photos/11150437/pexels-photo-11150437.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
          "landscape":
              "https://images.pexels.com/photos/11150437/pexels-photo-11150437.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
          "tiny":
              "https://images.pexels.com/photos/11150437/pexels-photo-11150437.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
        },
        "liked": false,
        "alt": "Afternoon break"
      });

      expect(obj.photographer, "Federica Gioia");
    });
  });
}
